#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_matrot_2x2() {
        let mut mat_in: [[u32; 2]; 2] = [
            [1, 2],
            [3, 4]
        ];
        let mat_exp: [[u32; 2]; 2] = [
            [3, 1],
            [4, 2]
        ];
        matrot(&mut mat_in);
        assert_eq!(mat_in, mat_exp);
    }

    #[test]
    fn test_matrot_3x3() {
        let mut mat_in: [[u32; 3]; 3] = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ];
        let mat_exp: [[u32; 3]; 3] = [
            [7, 4, 1],
            [8, 5, 2],
            [9, 6, 3]
        ];
        matrot(&mut mat_in);
        assert_eq!(mat_in, mat_exp);
    }

    #[test]
    fn test_matrot_4x4() {
        let mut mat_in: [[u32; 4]; 4] = [
            [1,   2,  3,  4],
            [5,   6,  7,  8],
            [9,  10, 11, 12],
            [13, 14, 15, 16]
        ];
        let mat_exp: [[u32; 4]; 4] = [
            [13,  9, 5, 1],
            [14, 10, 6, 2],
            [15, 11, 7, 3],
            [16, 12, 8, 4]
        ];
        matrot(&mut mat_in);
        assert_eq!(mat_in, mat_exp);
    }

    #[test]
    fn test_matrot_5x5() {
        let mut mat_in: [[u32; 5]; 5] = [
            [1,   2,  3,  4, 17],
            [5,   6,  7,  8, 18],
            [9,  10, 11, 12, 19],
            [13, 14, 15, 16, 20],
            [25, 24, 23, 22, 21],
        ];
        let mat_exp: [[u32; 5]; 5] = [
            [25, 13,  9,  5,  1],
            [24, 14, 10,  6,  2],
            [23, 15, 11,  7,  3],
            [22, 16, 12,  8,  4],
            [21, 20, 19, 18, 17]
        ];
        matrot(&mut mat_in);
        assert_eq!(mat_in, mat_exp);
    }

}

fn print_mat<const N: usize>(mat: &[[u32; N]; N]) {
    let mut s = String::new();
    for row in 0..N {
        for col in 0..N {
            let c = mat[row][col].to_string();
            s.push_str(c.as_str());
            s.push('\t');
        }
        s.push('\n');
    }
    println!("{}", s);
}

pub fn matrot<const N: usize>(mat: &mut [[u32; N]; N] ) {
    if N <= 1 {
        return;
    }
    let n_layers = (N + N%2) / 2;
    for layer in 0..n_layers {
        let end = N-1;
        for i in layer..end-layer {
            // Handle 4 at a time
            let t = mat[layer][i];
            let r = mat[i][end-layer];
            let b = mat[end-layer][end-i];
            let l = mat[end-i][layer];

            mat[layer][i] = l;
            mat[i][end-layer] = t;
            mat[end-layer][end-i] = r;
            mat[end-i][layer] = b;
        }
        print_mat(mat);
    }
}
